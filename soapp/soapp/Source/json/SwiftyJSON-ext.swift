//
//  SwiftyJSON-ext.swift
//  XPApp
//
//  Created by zhuxietong on 2017/3/20.
//  Copyright © 2017年 zhuxietong. All rights reserved.
//

import UIKit
import SwiftyJSON


extension NSDictionary
{
    public var rawJson:String?
        {
        get{
            return JSON(self).rawString([.jsonSerialization:JSONSerialization.WritingOptions.init(rawValue: 0)])
        }
    }
}

extension Dictionary
{
    public var rawJson:String?
        {
        get{
            return JSON(self).rawString([.jsonSerialization:JSONSerialization.WritingOptions.init(rawValue: 0)])
        }
    }
}

extension NSArray
{
    public var rawJson:String?
        {
        get{
            return JSON(self).rawString([.jsonSerialization:JSONSerialization.WritingOptions.init(rawValue: 0)])
        }
    }
}

extension Array{
    public var rawJson:String?
    {
        get{
            return JSON(self).rawString([.jsonSerialization:JSONSerialization.WritingOptions.init(rawValue: 0)])
        }
    }
}

