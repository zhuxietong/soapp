//
//  ControllerStyle.swift
//  JoTravel
//
//  Created by tong on 15/10/23.
//  Copyright © 2015年 zhuxietong. All rights reserved.
//

import UIKit

private var alphaViewKey = "alphaViewKey"
extension UINavigationController {
    var conav: CALayer{
        set {
            objc_setAssociatedObject(self, &alphaViewKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            if let v = objc_getAssociatedObject(self, &alphaViewKey) as? CALayer
            {
                return v
            }
            else{
                
                let lay = CALayer()

                
                objc_setAssociatedObject(self, &alphaViewKey, lay, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                navigationBar.setBackgroundImage(UIImage(), for: .default)
                navigationBar.shadowImage = UIImage()
//                navigationBar.insertSubview(alphaView, at: 0)
                
//                let aview = navigationBar.subviews[0]
                
                lay.frame = CGRect(x: 0.0, y: -20.0, width: UIScreen.main.bounds.width, height: 64.0)
//                navigationBar.layer.addSublayer(lay)
                if let l = navigationBar.layer.sublayers?[0]
                {
                    navigationBar.layer.insertSublayer(lay, below: l)
                }
              

                return lay
            }
        }
        
    }
    
}


private func createDefaultStyle()->ControllerStyle{
    let style = ControllerStyle()
    let color = UIColor.white
    style.nav_tinyColor = color
    style.navTitleAttributes = [NSForegroundColorAttributeName:color]
    style.nav_barColor = UIColor.black
    style.opacityNavBar = false
    style.statusBarStyle = UIStatusBarStyle.lightContent
    return style
}

public class ControllerStyle: NSObject {
    public var statusBarStyle = UIStatusBarStyle.default
    public var opacityNavBar = false
    

    public var navTitleAttributes : [String:AnyObject] = [NSForegroundColorAttributeName:UIColor.white]
    public var nav_tinyColor:UIColor = .white
    public var nav_barColor = UIColor.white
    public var hidenStatusBar = false
    
    public static var `default` = createDefaultStyle()
    
    public static var valid = false
    
    
//    class func DefaultStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor.white
//        style.nav_tinyColor = color
//        style.navTitleAttributes = [NSForegroundColorAttributeName:color]
//        style.nav_barColor = UIColor.black
//        style.opacityNavBar = false
//        style.statusBarStyle = UIStatusBarStyle.lightContent
//
//        return style
//    }
//
//    class func HomeStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor.white
//        style.nav_tinyColor = color
//        style.navTitleAttributes = [NSForegroundColorAttributeName:color]
//        style.nav_barColor = UIColor.clear
//        style.opacityNavBar = true
//        style.statusBarStyle = UIStatusBarStyle.lightContent
//        return style
//    }
//    
//    
//    class func LoginStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor.white
//        style.nav_tinyColor = color
//        style.navTitleAttributes = [NSForegroundColorAttributeName:color]
//        style.nav_barColor = UIColor.white
//        style.opacityNavBar = true
//        style.statusBarStyle = UIStatusBarStyle.lightContent
//        return style
//    }
//    
//    
//    
//    class func OrigenStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor(shex: "#FA9726")
//        style.nav_tinyColor = color
//        style.navTitleAttributes = [NSForegroundColorAttributeName:color]
//        style.nav_barColor = UIColor.white
//        style.opacityNavBar = false
//        style.statusBarStyle = UIStatusBarStyle.default
//        return style
//    }
//    
//    class func BlueBackStyle () -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor(shex: "#41B6FC")
//        style.nav_tinyColor = UIColor.white
//        style.navTitleAttributes = [NSForegroundColorAttributeName:color]
//        style.nav_barColor = color
//        style.opacityNavBar = false
//        style.statusBarStyle = UIStatusBarStyle.lightContent
//        return style
//    }
//    
//    class func OrigenBackStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor(shex: "#FA9726")
//        style.nav_tinyColor = UIColor.white
//        style.navTitleAttributes = [NSForegroundColorAttributeName:UIColor.white]
//        style.nav_barColor = color
//        style.opacityNavBar = false
//        style.statusBarStyle = UIStatusBarStyle.lightContent
//        return style
//    }
//    
//    class func White_Black_tyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor(shex: "#FFFFFF")
//        style.nav_tinyColor = UIColor.darkGray
//        style.navTitleAttributes = [NSForegroundColorAttributeName:UIColor.darkGray]
//        style.nav_barColor = color
//        style.opacityNavBar = false
//        style.statusBarStyle = UIStatusBarStyle.default
//        return style
//    }
//    //攻略
//    class func BlackBackStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor(shex: "#FA9726")
//        style.nav_tinyColor = color
//        style.navTitleAttributes = [NSForegroundColorAttributeName:color]
//        style.nav_barColor = UIColor.black
//        style.opacityNavBar = false
//        style.statusBarStyle = UIStatusBarStyle.lightContent
//        return style
//    }
//    
//    
//    class func AlphaNavStyle() -> ControllerStyle {
//        let style = ControllerStyle()
//        let color = UIColor.white
//        style.nav_tinyColor = color
//        style.navTitleAttributes = [NSForegroundColorAttributeName:UIColor.clear]
//        style.nav_barColor = UIColor.white
//        style.opacityNavBar = true
//        style.statusBarStyle = UIStatusBarStyle.default
//        return style
//    }
    

}



extension UIViewController
{
    private struct ExtentionKeys {
        static var controller_style_key = "controller_style_key"
    }
    public var controller_style: ControllerStyle? {
        get {
            if let obj = objc_getAssociatedObject(self, &ExtentionKeys.controller_style_key) as? ControllerStyle
            {
                return obj
            }
            else
            {
                return nil
            }
        }
        set {
            objc_setAssociatedObject(self, &ExtentionKeys.controller_style_key, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            self.navigationController?.setNeedsStatusBarAppearanceUpdate()
        }
    }
}



extension UINavigationController
{
    private struct NavExtentionKeys {
        static var navcontoller_style_key = "navcontoller_style_key"
    }
    
    
    
    public var nav_ctr_style: ControllerStyle {
        get {
            if let obj = objc_getAssociatedObject(self, &NavExtentionKeys.navcontoller_style_key) as? ControllerStyle
            {
                return obj
            }
            else
            {
                return ControllerStyle.default
            }
            
        }
        set {
            objc_setAssociatedObject(self, &NavExtentionKeys.navcontoller_style_key, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            self.updateControllerStyle()
        }
        
        
    }
    
    
    func updateControllerStyle()
    {
        self.setNeedsStatusBarAppearanceUpdate()
        
        
        weak var wself = self
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                
                if let ws = wself{
                    ws.navigationBar.barTintColor = ws.nav_ctr_style.nav_barColor
                    ws.navigationBar.tintColor = ws.nav_ctr_style.nav_tinyColor
                    ws.navigationBar.titleTextAttributes = ws.nav_ctr_style.navTitleAttributes
                    ws.conav.backgroundColor = ws.nav_ctr_style.nav_barColor.cgColor
                }
                
                }, completion: { (finish) -> Void in
//                    self.navigationBar.barTintColor = self.nav_ctr_style.nav_barColor
//                    self.navigationBar.tintColor = self.nav_ctr_style.nav_tinyColor
//                    self.navigationBar.titleTextAttributes = self.nav_ctr_style.navTitleAttributes
//                    self.navigationBar.backgroundColor = UIColor.clear
//                    self.conav.backgroundColor = self.nav_ctr_style.nav_barColor
                    
                    if let ws = wself{
                        if let l = ws.navigationBar.layer.sublayers?[0]
                        {
                            ws.navigationBar.layer.insertSublayer(ws.conav, below: l)
                        }
                    }
            })
    }

    
    
    
    
    
}






