//
//  CoLay.swift
//  jocool
//
//  Created by tong on 16/8/15.
//  Copyright © 2016年 zhuxietong. All rights reserved.
//

import UIKit



//public class JoPaw {
//    
//    
//    
//}
//
//
//
public extension JoPaw{
    
    
    public static func strings<T:Sequence>(list:T) ->[String]
    {
        var str_values = [String]()
        for t in list
        {
            str_values.append("\(t)")
        }
        return str_values
    }
}


open class So {
    
    public class lay{
        var priority:Float = 800
        var value:NSLayoutAttribute
        
        init(v:NSLayoutAttribute,priority:Float=800){
            self.value = v
            self.priority = priority
        }
    }
    
    public class map{
        var values:[lay] = [lay]()
        
        init(v:NSLayoutAttribute,p:Float=800){
            self.values.append(lay(v: v, priority: p))
        }
       
        public func priority(p:Float) -> map {
             self.values.last?.priority = p
            return self
        }
        public func p(_ p:Float) -> map {
            self.values.last?.priority = p
            return self
        }
        
        
        public var T:So.map{
            self.values.append(lay(v: .top))
            return self
        }
        public var B:So.map{
             self.values.append(lay(v: .bottom))
            return self
        }
        public var L:So.map{
            self.values.append(lay(v: .left))
            return self
        }
        public var R:So.map{
            self.values.append(lay(v: .right))
            return self
        }
        public var X:So.map{
            self.values.append(lay(v: .centerX))
            return self
        }
        public var Y:So.map{
            self.values.append(lay(v: .centerY))
            return self
        }
        
        
        public var width:So.map{
            self.values.append(lay(v: .width))
            return self
        }
        
        public var height:So.map{
            self.values.append(lay(v: .height))
            return self
        }
        
//        public static var bottom = map(v: NSLayoutAttribute.bottom)
//        public static var left = map(v: NSLayoutAttribute.left)
//        public static var right = map(v: NSLayoutAttribute.right)
//        public static var centerX = map(v: NSLayoutAttribute.centerX)
//        public static var centerY = map(v: NSLayoutAttribute.centerY)
        
        
        
    }
//    
//    public static var top = map(v: NSLayoutAttribute.top)
//    public static var bottom = map(v: NSLayoutAttribute.bottom)
//    public static var left = map(v: NSLayoutAttribute.left)
//    public static var right = map(v: NSLayoutAttribute.right)
//    public static var centerX = map(v: NSLayoutAttribute.centerX)
//    public static var centerY = map(v: NSLayoutAttribute.centerY)
//    
    
    
    public static var T:So.map{
        return map(v: NSLayoutAttribute.top)
    }
    public static var B:So.map{
        return map(v: NSLayoutAttribute.bottom)
    }
    public static var L:So.map{
        return map(v: NSLayoutAttribute.left)
    }
    public static var R:So.map{
        return map(v: NSLayoutAttribute.right)
    }
    public static var X:So.map{
        return map(v: NSLayoutAttribute.centerX)
    }
    public static var Y:So.map{
        return map(v: NSLayoutAttribute.centerY)
    }
    
    
    public static var width:So.map{
        return map(v: NSLayoutAttribute.width)
    }
    
    public static var height:So.map{
        return map(v: NSLayoutAttribute.height)
    }
    
    
    public static var none:So.map{
        return map(v: NSLayoutAttribute.notAnAttribute)
    }

}






//public struct TP {
//    
//}
public extension TP
{
    typealias lays = [[Any]]
}




extension String
{
    var cg_float:CGFloat{
        get{
            let str = NSString(string: self)
            return CGFloat(str.floatValue)
        }
    }
}





public extension UIView{
    
    public var solay:TP.lays {
        set(newValue){
           _ = UIView.solay(lays: newValue, at: self)
        }
        get{
            return TP.lays()
        }
    }
    
    
    @discardableResult
    class public func solay<T:UIView>(lays:TP.lays,at:T)->([[NSLayoutConstraint]],[NSLayoutConstraint]) {
        
        var constrains = [[NSLayoutConstraint]]()
        if lays.count > 0
        {
            for oneLays in lays{
                if let one_view = oneLays[0] as? UIView
                {
                    one_view.translatesAutoresizingMaskIntoConstraints = false
                    
                    if one_view !== at
                    {
                        if let o = one_view.superview
                        {
                            if o !== at
                            {
                                at.addSubview(one_view)
                            }
                        }
                        else
                        {
                            at.addSubview(one_view)
                        }
                    }
                }
            }
            
            
            for oneLays in lays{
                var one_constains = [NSLayoutConstraint]()
                
                var t_view:UIView = at
                
                var item_lays = oneLays
                if let t_view_1 = item_lays[0] as? UIView
                {
                    t_view = t_view_1
                    
                    
                    item_lays.remove(at: 0)
                    //                    at.addSubview(t_view)
                    
                }
                
                for one in item_lays
                {
                    
                    if let w = one as? Int
                    {
                        //宽度
                        let c =  NSLayoutConstraint(item: t_view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.width, multiplier: 1, constant: CGFloat(w))
                        c.priority = 999
                        
                        one_constains.append(c)
                        at.addConstraint(c)
                        continue
                        
                    }
                    if let w = one as? Double
                    {
                        //宽度
                        let c =  NSLayoutConstraint(item: t_view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.width, multiplier: 1, constant: CGFloat(w))
                        c.priority = 999
                        
                        one_constains.append(c)
                        at.addConstraint(c)
                        continue
                        //                        (t_view,.Width) |=| (nil,.Width,w.cg_floatValue)
                    }
                    
                    if let w = one as? CGFloat
                    {
                        //宽度
                        let c =  NSLayoutConstraint(item: t_view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.width, multiplier: 1, constant: w)
                        c.priority = 999
                        
                        one_constains.append(c)
                        at.addConstraint(c)
                        continue
                        
                        //                        (t_view,.Width) |=| (nil,.Width,w.cg_floatValue)
                    }
                    
                    
                    if let h = one as? String
                    {
                        //高度
                        let c =  NSLayoutConstraint(item: t_view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.height, multiplier: 1, constant: h.cg_float)
                        c.priority = 999
                        one_constains.append(c)
                        t_view.superview!.addConstraint(c)
                        continue
                        //                        (t_view,.Height) |=| (nil,.Height,h.cg_floatValue)
                    }
                    
                    
                    if let alay = one as? NSArray
                    {
                     
                        
                        var relate_v = t_view.superview
                        let one_lay:NSMutableArray = NSMutableArray(array: alay)
                        if  let v = alay[0] as? UIView
                        {
                            relate_v = v
                            one_lay.removeObject(at:0)
                        }
                        let co_count = Node<So.map>.count(list: one_lay)
                        
                        var to_atr = So.none
                        var this_atr = So.none
                        
                        if co_count == 1
                        {
                            to_atr = one_lay[0] as! So.map
                            this_atr = one_lay[0] as! So.map
                        }
                        if co_count == 2
                        {
                            to_atr = one_lay[0] as! So.map
                            this_atr = one_lay[1] as! So.map
                        }
                        //取值
                        var value:String = "0"
                        if let v = one_lay.lastObject as? Int
                        {
                            value = "\(v)"
                        }
                        if let v = one_lay.lastObject as? Double
                        {
                            value = "\(v)"
                        }
                        if let v = one_lay.lastObject as? String
                        {
                            value = "\(v)"
                        }
                        if let v = one_lay.lastObject as? CGFloat
                        {
                            value = "\(v)"
                        }
                        if let v = one_lay.lastObject as? [Int]
                        {
                            let strs = JoPaw.strings(list: v)
                            value = strs.joined(separator: "|")
                        }
                        if let v = one_lay.lastObject as? [Double]
                        {
                            let strs = JoPaw.strings(list: v)
                            value = strs.joined(separator:"|")
                        }
                        if let v = one_lay.lastObject as? [CGFloat]
                        {
                            let strs = JoPaw.strings(list: v)
                            value = strs.joined(separator:"|")
                        }
                        if let v = one_lay.lastObject as? [String]
                        {
                            let strs = JoPaw.strings(list: v)
                            value = strs.joined(separator:"|")
                        }
                        
                        
                        var _values = value.components(separatedBy: "|")
                        
                        
                        if to_atr.values.count >= this_atr.values.count
                        {
                            for (i,lay0) in this_atr.values.enumerated()
                            {

                                if lay0.value != NSLayoutAttribute.notAnAttribute
                                {
                                    let lay1 = to_atr.values[i]
                                    
                                    var _v = "0"
                                    if _values.count > i{
                                        _v = _values[i]
                                    }
                                    else if _values.count > 0
                                    {
                                        _v = _values[0]
                                    }
                                    else{}
                                    
                                    
                                    let x =  NSLayoutConstraint(item: t_view, attribute: lay0.value, relatedBy: NSLayoutRelation.equal, toItem: relate_v, attribute: lay1.value, multiplier: 1, constant: _v.cg_float)
                                    x.priority = lay0.priority
                                    one_constains.append(x)
                                    t_view.superview!.addConstraint(x)
                                    
                                }
                            }
                        }
                    }
                }
                constrains.append(one_constains)
            }
        }
        
        var cons = [NSLayoutConstraint]()
        
        for cs in constrains
        {
            for one_c in cs
            {
                cons.append(one_c)
            }
        }
        
        return (constrains,cons)
    }
}




 
